package meinErstesSpiel.inputHandler;

import javafx.scene.input.KeyCode;
import meinErstesSpiel.model.Model;

public class InputHandler {

    private Model model;

    public InputHandler(Model model) {
        this.model = model;
    }

    public void onKeyPressed(KeyCode keycode){

        if (keycode == KeyCode.UP || keycode == KeyCode.W){
            model.movePlayer(0, -10);
        }
        if (keycode == KeyCode.DOWN || keycode == KeyCode.S){
            model.movePlayer(0, 10);
        }
        if (keycode == KeyCode.LEFT || keycode == KeyCode.A){
            model.movePlayer(-10, 0);
        }
        if (keycode == KeyCode.RIGHT || keycode == KeyCode.D){
            model.movePlayer(10, 0);
        }
        if (keycode == KeyCode.U){
            model.nextLevel();
        }
        if (keycode == KeyCode.Y){
            model.newGame();
        }
        if (keycode == KeyCode.N){
            model.quit();
        }


    }
}
