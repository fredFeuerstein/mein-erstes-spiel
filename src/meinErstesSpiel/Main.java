package meinErstesSpiel;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;
import meinErstesSpiel.graphics.Graphics;
import meinErstesSpiel.inputHandler.InputHandler;
import meinErstesSpiel.model.Model;
import meinErstesSpiel.timer.Timer;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage)  {

        primaryStage.setTitle("Pac in the Snow");

        Canvas canvas = new Canvas(800, 800);
        Model model = new Model();

        Group group = new Group();
        group.getChildren().add(canvas);
        Scene scene = new Scene(group);
        primaryStage.setScene(scene);

        GraphicsContext gc = canvas.getGraphicsContext2D();

        Graphics graphics = new Graphics(gc, model);

        Timer timer = new Timer(graphics, model);
        timer.start();

        InputHandler inputHandler = new InputHandler(model);

        scene.setOnKeyPressed(
                event -> inputHandler.onKeyPressed(event.getCode())
        );

        primaryStage.show();
    }

}
