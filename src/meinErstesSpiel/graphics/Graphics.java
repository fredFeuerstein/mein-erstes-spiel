package meinErstesSpiel.graphics;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import meinErstesSpiel.model.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Graphics {

    private GraphicsContext gc;
    private Model model;
    private Image background;
    private Image obstacles;
    private Image playerRightPic;
    private Image playerDownPic;
    private Image playerLeftPic;
    private Image playerUpPic;
    private Image enemyPic;
    private Image enemyBigPic;
    private Image enemyBiggerPic;
    private Image enemyBiggestPic;
    private Image banditPic;
    private Image banditHitPic;
    private Image homePic;
    private Image dollarPic;
    private Image snowflakePic;


    public Graphics(GraphicsContext gc, Model model) {

        this.gc = gc;
        this.model = model;

        try {
            background = new Image(new FileInputStream("resources/background_snow.jpeg"));
            obstacles = new Image(new FileInputStream("resources/rock.jpg"));
            playerRightPic = new Image(new FileInputStream("resources/Pac_Man_right_transp.png"));
            playerDownPic = new Image(new FileInputStream("resources/Pac_Man_down_transp.png"));
            playerLeftPic = new Image(new FileInputStream("resources/Pac_Man_left_transp.png"));
            playerUpPic = new Image(new FileInputStream("resources/Pac_Man_up_transp.png"));
            enemyPic = new Image(new FileInputStream("resources/ghost_transp.png"));
            enemyBigPic = new Image(new FileInputStream("resources/ghost_big_transp.png"));
            enemyBiggerPic = new Image(new FileInputStream("resources/ghost_bigger_transp.png"));
            enemyBiggestPic = new Image(new FileInputStream("resources/ghost_biggest_transp.png"));
            banditPic = new Image(new FileInputStream("resources/bandit_transp.png"));
            banditHitPic = new Image(new FileInputStream("resources/bandit_hit_transp.png"));
            homePic = new Image(new FileInputStream("resources/HomeMadeByLisa.JPG"));
            dollarPic = new Image(new FileInputStream("resources/Dollar_transp.png"));
            snowflakePic = new Image(new FileInputStream("resources/snowflake_transp.png"));
        } catch (FileNotFoundException e){
            System.out.println("At least one file was not found.");
        }

    }

    public void draw() {

        gc.clearRect(0, 0, model.WIDTH, model.HEIGHT);
        gc.drawImage(background, 0, 0);

        drawPlayground();

        drawUpperField();

        drawLowerField();

        drawWinLost();

        for (Snowflake snowflake : model.getSnowflakes()) {
            gc.drawImage(snowflakePic, snowflake.getX(), snowflake.getY());
        }

    }

    private void drawPlayer(){

        if (model.getPlayer().getPosition().equals("right")) {
            gc.drawImage(playerRightPic, model.getPlayer().getX(), model.getPlayer().getY());

        } else if (model.getPlayer().getPosition().equals("down")) {
            gc.drawImage(playerDownPic, model.getPlayer().getX(), model.getPlayer().getY());

        } else if (model.getPlayer().getPosition().equals("left")) {
            gc.drawImage(playerLeftPic, model.getPlayer().getX(), model.getPlayer().getY());

        } else if (model.getPlayer().getPosition().equals("up")) {
            gc.drawImage(playerUpPic, model.getPlayer().getX(), model.getPlayer().getY());

        }
    }

    private void drawEnemy(){
        //Enemies
        for (Foe enemy : model.getEnemies()) {

            int i = enemy.getHit();

            switch (i) {
                case 0:
                    gc.drawImage(enemyPic, enemy.getX(), enemy.getY());
                    break;
                case 1:
                    gc.drawImage(enemyBigPic, enemy.getX() - 75, enemy.getY() - 75);
                    model.setBreakTime(500);
                    enemy.setHit();
                    return;

                case 2:
                    int x = enemy.getX();
                    int y = enemy.getY();

                    if (x - 300 > 0 && x -300 < 200){
                        x -= 300;
                    } else if (x - 300 > 200){
                        x = 200;
                    } else {
                        x = 0;
                    }

                    if (y - 300 > 0 && y -300 < 200){
                        y -= 300;
                    } else if (y - 300 > 200){
                        y = 200;
                    } else {
                        y = 0;
                    }

                    gc.drawImage(enemyBiggerPic, x , y);
                    model.setBreakTime(500);
                    enemy.setHit();
                    return;
                case 3:
                    gc.drawImage(enemyBiggestPic, 0, 0);
                    model.setBreakTime(1000);
                    enemy.resetHit();
                    return;

            }
        }

        //Bandits
        for (Foe bandit : model.getBandits()) {
            if(bandit.getHit() == 0) {
                gc.drawImage(banditPic, bandit.getX(), bandit.getY());
            }
            if(bandit.getHit() == 1){
                gc.drawImage(banditHitPic, bandit.getX(), bandit.getY());
                model.setBreakTime(300);
                bandit.resetHit();
            }
        }
    }

    private void drawUpperField(){
        //Time
        gc.setFill(Color.BLACK);
        gc.setTextAlign(TextAlignment.LEFT);
        gc.setFont(Font.font("Algerian", FontWeight.BOLD, 20));
        gc.fillText("Time: " + model.getCountdown() + " s", 100, 60);


        //Guideline
        if(model.getPlayer().getCoins() < 5){

            gc.setFill(Color.BLACK);
            gc.setTextAlign(TextAlignment.CENTER);
            gc.setFont(Font.font("Algerian", FontWeight.BOLD, 20));
            gc.fillText("Get the coins!", 400, 60);
        } else {
            gc.setFill(Color.BLACK);
            gc.setTextAlign(TextAlignment.CENTER);
            gc.setFont(Font.font("Algerian", FontWeight.BOLD, 20));
            gc.fillText("Go home!", 400, 60);
        }

        //Coincounter
        gc.setFill(Color.BLACK);
        gc.setTextAlign(TextAlignment.RIGHT);
        gc.setFont(Font.font("Algerian", FontWeight.BOLD, 20));
        gc.fillText("Coins: " + model.getPlayer().getCoins() + "/" + model.getCoinGoal(), 700, 60);

    }

    private void drawLowerField(){
        //Level
        gc.setFill(Color.BLACK);
        gc.setTextAlign(TextAlignment.LEFT);
        gc.setFont(Font.font("Algerian", FontWeight.BOLD, 25));
        gc.fillText("Level: " + model.getLevel(), 100, 740);

        //Lifecounter
        gc.setFill(Color.BLACK);
        gc.setTextAlign(TextAlignment.RIGHT);
        gc.setFont(Font.font("Algerian", FontWeight.BOLD, 25));
        gc.fillText("Lives: " + model.getPlayer().getLife(), 700, 740);

    }

    private void drawWinLost(){
        //Win
        if (model.isWin()) {
            gc.setFill(Color.BLACK);
            gc.setTextAlign(TextAlignment.CENTER);
            gc.setFont(Font.font("Algerian", FontWeight.BOLD, 40));
            gc.fillText("YOU WIN!", 400, 400);
        }

        //Lost
        if (model.isLost()) {
            gc.setFill(Color.BLACK);
            gc.fillRect(100, 300, 600, 200);
            gc.setFill(Color.DEEPSKYBLUE);
            gc.setTextAlign(TextAlignment.CENTER);
            gc.setFont(Font.font("Algerian", FontWeight.BOLD, 40));
            gc.fillText("GAME OVER", 400, 380);
            gc.fillText("START A NEW GAME? Y / N", 400, 440);
        }
    }

    private void drawPlayground(){
        gc.setFill(Color.WHITE);
        gc.fillRect(100, 100, 600, 600);

        //Obstacles
        for (Obstacle obstacle : model.getObstacles()) {
            gc.drawImage(obstacles, obstacle.getX(), obstacle.getY());
        }

        //Dollars
        for (Coin coin : model.getCoins()) {
            gc.drawImage(dollarPic, coin.getX(), coin.getY());
        }

        drawPlayer();

        //Goal
        gc.drawImage(homePic, model.getGoal().getX(), model.getGoal().getY());

        drawEnemy();
    }
}
