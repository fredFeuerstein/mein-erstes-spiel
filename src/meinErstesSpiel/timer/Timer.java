package meinErstesSpiel.timer;

import javafx.animation.AnimationTimer;
import meinErstesSpiel.graphics.Graphics;
import meinErstesSpiel.model.Model;

public class Timer extends AnimationTimer {

    private Graphics graphics;
    private Model model;

    public Timer(Graphics graphics, Model model) {
        this.graphics = graphics;
        this.model = model;
    }

    @Override
    public void handle(long nowNano) {
        long milliSec = nowNano / 1000000;

        this.pause(model.getBreakTime());
        model.update(milliSec);
        graphics.draw();
    }

    public void pause(int breakTime){

        try {
            Thread.sleep(breakTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        model.setBreakTime(0);
    }
}
