package meinErstesSpiel.model;

import java.util.ArrayList;
import java.util.Random;

public class Model {

    private ArrayList<Obstacle> obstacles = new ArrayList();
    private ArrayList<Coin> coins = new ArrayList<>();
    private ArrayList<Foe> enemies = new ArrayList<Foe>();
    private ArrayList<Foe> bandits = new ArrayList<Foe>();
    private ArrayList<Snowflake> snowflakes = new ArrayList<>();
    private Player player;
    private Goal goal;
    private boolean win;
    private boolean lost;
    private boolean newGame;
    private long previousTime;
    private int coinGoal;
    private int level;
    private int numberOfEnemies;
    private int numberOfBandits;
    private int breakTime;
    private int countdown;
    private double quarterSec;
    private int eighthSec;
    private double second;
    public final int WIDTH;
    public final int HEIGHT;
    private Random random;

    public Model() {
        this.WIDTH = 600;
        this.HEIGHT = 600;
        this.goal = new Goal(670, 670);
        this.player = new Player(100, 100);
        this.win = false;
        this.lost = false;
        this.newGame = false;
        this.coinGoal = 5;
        this.level = 1;
        this.numberOfEnemies = 10;
        this.numberOfBandits = 4;
        this.breakTime = 0;
        this.countdown = 120;
        this.eighthSec = 0;
        this.quarterSec = 0;
        this.random = new Random();
        this.setAll();
    }

    public void resetCoins() {
        this.player.setCoins(0);
        this.coins.clear();
    }


    public void update(long milliSec) {

        if (win) {
            nextLevel();
        }

        if (newGame) {
            this.newGame();
        }

        if ((player.getX() == goal.getX() && player.getY() == goal.getY()) && player.getCoins() == coinGoal && !lost) {
            win = true;
            this.breakTime = 2000;
        }

        if (this.player.getLife() == 0 || this.countdown == 0) {
            lost = true;
            this.player.resetLife();
        }

        if (quarterSec == 4) {
            countdown--;
            quarterSec = 0;
        }
        if (eighthSec == 2) {
            moveFoe(enemies);
            moveFoe(bandits);
            eighthSec = 0;
        }
        if(milliSec > previousTime + 125){

            for (int i = 0; i < snowflakes.size(); i++) {
                snowflakes.get(i).move();
                if(snowflakes.get(i).getY() > 800){
                    snowflakes.remove(i);
                }
            }
            this.snowflakes.add(new Snowflake(random.nextInt(800), 0));
            previousTime = milliSec;
            eighthSec++;
            quarterSec += 0.5;
            if(player.isInvincible()){
                second += 0.125;
            }
        }

        //Collision with enemy
        if(!player.isInvincible()){

            for (Foe enemy : enemies) {

                if (collisionPlayer(enemy)) {
                    enemy.setHit();
                    player.moveTo(100, 100);
                    player.loseLife();
                }
            }
        }

        if(player.isInvincible() && second >= 4){
            player.setInvincible(false);
            second = 0;
        }

        //Collision with bandit
        for (Foe bandit : bandits) {


            if (collisionPlayer(bandit) && player.getCoins() > 0) {

                bandit.setHit();
                resetCoins();

                //Set coins
                this.setObjects("coin", this.coinGoal);
            }
        }

        //Collision with coin
        for (int i = 0; i < coins.size(); i++) {

            if (collisionPlayer(coins.get(i))) {
                coins.remove(i);
                player.setCoins();
            }
        }
    }

    private boolean collisionPlayer(Figure figure){
        int playerX = player.getX() + 15;
        int playerY = player.getY() + 15;
        int figureX = figure.getX() + 20;
        int figureY = figure.getY() + 20;

        int dx2 = Math.abs(playerX - figureX);
        int dy2 = Math.abs(playerY - figureY);

        return dx2 < 35 && dy2 < 35;
    }



    public void movePlayer(int dx, int dy) {
        if (moveAllowed(player, dx, dy)) {
            player.x += dx;
            player.y += dy;

            if (dx > 0) {
                player.setPosition("right");
            } else if (dx < 0) {
                player.setPosition("left");
            } else if (dy > 0) {
                player.setPosition("down");
            } else if (dy < 0) {
                player.setPosition("up");
            }
        }

    }

    private void moveFoe(ArrayList<Foe> foeList) {

        boolean positioned = false;

        for (Foe foe : foeList) {

            do {
                int k = random.nextInt(4) + 1;

                switch (k) {

                    case 1:
                        if (moveAllowed(foe, 5, 0)) {
                            foe.move(5, 0);
                            positioned = true;
                        }
                        break;
                    case 2:
                        if (moveAllowed(foe, 0, 5)) {
                            foe.move(0, 5);
                            positioned = true;
                        }
                        break;
                    case 3:
                        if (moveAllowed(foe, -5, 0)) {
                            foe.move(-5, 0);
                            positioned = true;
                        }
                        break;
                    case 4:
                        if (moveAllowed(foe, 0, -5)) {
                            foe.move(0, -5);
                            positioned = true;
                        }
                        break;
                }
            } while (!positioned);
        }
    }

    private boolean moveAllowed(Item item, int dx, int dy) {

        if (item.x + dx >= 100 && item.x + 30 + dx <= WIDTH + 100 && item.y + dy >= 100 && item.y + 30 + dy <= HEIGHT + 100) {

            for (Obstacle obstacle : obstacles) {

                int objectX = item.getX() + 15;
                int objectY = item.getY() + 15;
                int obstacleX = obstacle.getX() + 20;
                int obstacleY = obstacle.getY() + 20;

                int dx2 = Math.abs(objectX + dx - obstacleX);
                int dy2 = Math.abs(objectY + dy - obstacleY);

                if (dx2 < 35 && dy2 < 35) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    private boolean setAllowed(int x, int y) {

        if (x > 130 && y > 130 && x < 630 && y < 630 && x % 10 == 0 && y % 10 == 0) {
            for (Obstacle object : obstacles) {
                if (((x > object.x - 40 && x <= object.x + 40) && (y >= object.y - 40 && y <= object.y + 40))) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public void nextLevel() {

        this.level++;
        this.numberOfEnemies += 2;
        this.player.moveTo(100, 100);
        this.resetCoins();
        this.player.resetLife();
        this.win = false;
        this.countdown = 120;

        this.obstacles.clear();
        this.enemies.clear();

        //Set enemies, bandit, obstacles
        this.setAll();

        //Set bandits
        if (this.level >= 3) {

            this.bandits.clear();
            this.setObjects("bandit", this.numberOfBandits);
            numberOfBandits += 2;
        }


    }

    public void newGame() {

        if (lost) {
            this.level = 1;
            this.numberOfEnemies = 10;
            this.player.moveTo(100, 100);
            this.resetCoins();
            this.player.resetLife();
            this.win = false;
            this.lost = false;
            this.countdown = 120;

            this.obstacles.clear();
            this.enemies.clear();
            this.bandits.clear();

            //Set enemies, bandit, obstacles
            this.setAll();

        }
    }

    public void quit(){
        System.exit(0);
    }

    public void setAll(){
        this.setObjects("obstacle", 30);
        this.setObjects("enemy", this.numberOfEnemies);
        this.setObjects("coin", this.coinGoal);
    }

    public void setObjects(String listName, int amount){

        for(int i = 0; i < amount; ){

            int x = random.nextInt(WIDTH - 40) + 100;
            int y = random.nextInt(HEIGHT - 40) + 100;

            if (setAllowed(x, y)) {

                switch (listName){

                    case "coin":
                        coins.add(new Coin(x, y));
                        i++;
                        break;
                    case "enemy":
                        enemies.add(new Foe(x, y));
                        i++;
                        break;
                    case "bandit":
                        bandits.add(new Foe(x, y));
                        i++;
                        break;
                    case "obstacle":
                        obstacles.add(new Obstacle(x, y));
                        i++;
                        break;
                }
            }
        }
    }

    public Player getPlayer() {
        return player;
    }

    public Goal getGoal() {
        return goal;
    }

    public ArrayList<Obstacle> getObstacles() {
        return obstacles;
    }

    public ArrayList<Foe> getEnemies() {
        return enemies;
    }

    public ArrayList<Coin> getCoins() {
        return coins;
    }

    public ArrayList<Foe> getBandits() {
        return bandits;
    }

    public ArrayList<Snowflake> getSnowflakes() {
        return snowflakes;
    }

    public int getCoinGoal() {
        return coinGoal;
    }

    public int getLevel() {
        return level;
    }

    public int getBreakTime() {
        return breakTime;
    }

    public void setBreakTime(int breakTime) {
        this.breakTime = breakTime;
    }

    public int getCountdown() {
        return countdown;
    }

    public boolean isWin() {
        return win;
    }

    public boolean isLost() {
        return lost;
    }
}
