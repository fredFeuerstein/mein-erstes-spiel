package meinErstesSpiel.model;

public class Player extends Figure {

    private int life;
    private String position;
    private int coins;
    private boolean invincible;

    public Player(int x, int y){
        super(x, y);
        this.life = 3;
        this.position = "right";
        this.coins = 0;
        this.invincible = false;
    }

    public void moveTo(int x, int y){
        this.invincible = true;
        this.x = x;
        this.y = y;
    }

    public int getLife() {
        return life;
    }

    public void loseLife(){
        this.life--;
    }

    public void resetLife(){
        this.life = 3;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getCoins() {
        return coins;
    }

    public void setCoins() {
        this.coins++;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }

    public boolean isInvincible() {
        return invincible;
    }

    public void setInvincible(boolean invincible) {
        this.invincible = invincible;
    }
}
