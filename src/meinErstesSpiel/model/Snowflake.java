package meinErstesSpiel.model;

public class Snowflake extends Item {

    public Snowflake(int x, int y) {
        super(x, y, 11, 11);
    }

    public void move() {
        this.y += 10;
        this.x += 2;
    }


}
