package meinErstesSpiel.model;

public abstract class Figure extends Item {

    public Figure(int x, int y) {
        super(x, y, 30, 30);
    }
}
