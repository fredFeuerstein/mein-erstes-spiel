package meinErstesSpiel.model;

public abstract class Item {

    protected int x;
    protected int y;
    protected int h;
    protected int b;

    public Item(int x, int y, int h, int b) {
        this.x = x;
        this.y = y;
        this.h = h;
        this.b = b;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

}
