package meinErstesSpiel.model;

public class Foe extends Figure {

    private int hit;

    public Foe(int x, int y) {
        super(x, y);
        this.hit = 0;
    }

    public void move(int dx, int dy) {
        this.x += dx;
        this.y += dy;
    }

    public int getHit() {
        return hit;
    }

    public void setHit() {
        this.hit++;
    }

    public void resetHit(){
        this.hit = 0;
    }
}
